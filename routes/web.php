<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
})->name('dashboard');

Route::get('/recipes/browse', \App\Http\Livewire\ListRecipe::class)->name('recipes.browse');
Route::get('/recipes/show/{recipe}', \App\Http\Livewire\ShowRecipe::class)->name('recipes.show');

Route::middleware(['auth'])->group(function (){
    Route::get('/recipes/my-recipes', \App\Http\Livewire\MyRecipes::class)->name('recipes.my-recipes');
    Route::get('/recipes/create', \App\Http\Livewire\CreateRecipe::class)->name('recipes.create');
    Route::get('/recipes/edit/{recipe}', \App\Http\Livewire\EditRecipe::class)->name('recipes.edit');
});

require __DIR__.'/auth.php';

