<?php

namespace Database\Seeders;

use App\Models\Recipe;
use App\Models\RecipeStep;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    const CUSTOM_USER_EMAILS = [
        'dev@recipe.test'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createCustomUsers();
    }

    /**
     *  Create a dev user account with custom email
     */
    private function createCustomUsers()
    {
        collect(self::CUSTOM_USER_EMAILS)->each(fn($email) => User::factory()->create(['email' => $email]));

        // Create a Recipe for this User with some tags associated.
        $recipe = Recipe::factory()->for(User::where('email', 'dev@recipe.test')->first())->create();

        $steps = [];

        for($i = 0; $i <= 1; $i++){
            $steps[$i] = "step $i";
        }
//dd($steps);
        foreach($steps as $order => $description){
            RecipeStep::create([
                'recipe_id' => $recipe->id,
                'order' => $order,
                'description' => $description
            ]);
        }
    }
}
