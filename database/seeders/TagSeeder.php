<?php

namespace Database\Seeders;

use App\Models\Tag;
use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $seeder_tags = [
            'spicy',
            'mediterranean',
            'italian',
            'mexican',
            'american',
            'german',
            'milk',
            'russian',
            'mild',
            'breakfast'
        ];

        collect($seeder_tags)->each(function ($tag){
            Tag::firstOrCreate(['name' => $tag]);
        });
    }
}
