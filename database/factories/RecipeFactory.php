<?php

namespace Database\Factories;

use App\Models\Recipe;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Factories\Factory;

class RecipeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Recipe::class;

    public function configure(): RecipeFactory
    {
        return $this->afterCreating(function (Recipe $recipe) {
            $recipe->tags()->attach(Tag::inRandomOrder()->limit($this->faker->numberBetween(1, 12))->get());
        });
    }

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition(): array
    {
        return [
            'title' => $this->faker->sentence(),
            'description' => $this->faker->paragraph(),
            'prep_time' => $this->faker->randomElement(['15', '30', '60']),
            'cook_time' => $this->faker->randomElement(['15', '30', '45', '60']),
        ];
    }
}
