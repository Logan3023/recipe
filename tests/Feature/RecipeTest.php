<?php

namespace Tests\Feature;

use App\Http\Livewire\CreateRecipe;
use App\Http\Livewire\EditRecipe;
use App\Http\Livewire\MyRecipes;
use App\Models\Recipe;
use App\Models\RecipeStep;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Livewire\Livewire;
use Tests\TestCase;

class RecipeTest extends TestCase
{
    use WithFaker;

    /**
     * Test that a user can access the recipe creation view.
     *
     * @return void
     */
    public function testRegisteredUserCanAccessRecipeCreationPageAndStoreRecipe()
    {
        // Login as a User
        $this->actingAs($user = User::factory()->create());

        // Create an array of tags to be utilized for our POST request.
        $tags = ['american', 'dinner', 'spicy'];

        $steps = ['step one', 'step two', 'step three'];


        // Generate some sorta unique strings for assertions later in the test
        $title = $this->faker->sentence($this->faker->numberBetween(5, 15));
        $description = $this->faker->paragraph();

        // Instantiate our Livewire test using the CreateRecipe class and set the request data.
        $livewire = Livewire::test(CreateRecipe::class)
            ->set('title', $title)
            ->set('user_id', $user->id)
            ->set('tags', $tags)
            ->set('steps', $steps)
            ->set('description', $description)
            ->set('prep_time', $this->faker->numberBetween(5, 50))
            ->set('cook_time', $this->faker->numberBetween(5, 50));

        // Make the request to CreateRecipe@store
        $livewire->call('store');

        // Check that the Recipe was persisted, duh.
        $this->assertTrue(Recipe::whereTitle($title)->exists());

        // Get our new Recipe and eager-load Tags.
        $recipe = Recipe::whereTitle($title)->with('tags')->first();

        // Loop over the Recipe's Tags and assert that each one was persisted to the database and associated with the recipe
        collect($tags)->each(function ($tag) use ($recipe) {
            $this->assertTrue($recipe->tags()->whereName($tag)->exists());
        });
    }

    /**
     * A user can update their recipe.
     *
     * @return void
     */
    public function testRegisteredUserCanAccessRecipeEditPage()
    {
        // Login as a User
        $this->actingAs($user = User::factory()->create());

        // Create a new recipe to update later in the test and check against.
        $recipe = Recipe::factory()->for($user)->create();

        // Create some pre-existing steps for the Recipe so we can test over-riding later in the test.
        $preExistingSteps = [];
        for ($stepOrder = 0; $stepOrder < 10; $stepOrder++) {
            $preExistingSteps[$stepOrder] = $this->faker->sentence();
        }

        foreach ($preExistingSteps as $key => $value) {
            RecipeStep::create([
                'recipe_id' => $recipe->id,
                'order' => $key,
                'description' => $value[$key]
            ]);
        }

        // Create an array of tags to be utilized for our POST request.
        $tags = ['american', 'dinner', 'spicy'];

        $newSteps = [];
        for ($stepOrder = 0; $stepOrder < 3; $stepOrder++) {
            $newSteps[$stepOrder] = $this->faker->sentence();
        }

        // Generate some sorta unique strings for assertions later in the test
        $title = $this->faker->sentence($this->faker->numberBetween(5, 15));
        $description = $this->faker->paragraph();

        // Instantiate our Livewire test using the EditRecipe class and set the request data.
        $livewire = Livewire::test(EditRecipe::class, [$recipe])
            ->set('title', $title)
            ->set('tags', $tags)
            ->set('steps', $newSteps)
            ->set('description', $description)
            ->set('prep_time', $this->faker->numberBetween(5, 50))
            ->set('cook_time', $this->faker->numberBetween(5, 50));

        // Make the request to EditRecipe@update
        $livewire->call('update')->assertStatus(200);

        // Refresh our Recipe model real quick
        $recipe->refresh();

        // Check that the Recipe was persisted, duh.
        $this->assertTrue(Recipe::whereTitle($title)->exists());

        // Loop over the Recipe's Tags and assert that each one was persisted to the database and associated with the recipe
        collect($tags)->each(function ($tag) use ($recipe) {
            $this->assertTrue($recipe->tags()->whereName($tag)->exists());
        });

        // Loop over the Recipe's Steps and assert that each one was persisted to the database and associated with the recipe
        collect($newSteps)->each(function ($newStep) use ($recipe) {
            $this->assertTrue(RecipeStep::whereDescription($newStep)->exists());
            $this->assertTrue($recipe->steps()->whereDescription($newStep)->exists());
        });

        // Assert that the pre-existing steps were properly destroyed and disassociated with the Recipe.
        collect($preExistingSteps)->each(function ($preExistingStep) use ($recipe) {
            $this->assertNotTrue(RecipeStep::whereDescription($preExistingStep)->exists());
            $this->assertNotTrue($recipe->steps()->whereDescription($preExistingStep)->exists());
        });

        // Assert the changes to our Recipe model have persisted
        $this->assertEquals($recipe->title, $title);
        $this->assertEquals($recipe->description, $description);
    }

    /**
     * A user can view their recipes.
     *
     * @return void
     */
    public function testAuthenticatedUsersCanAccessMyRecipesPage()
    {
        // Login as a User
        $this->actingAs($user = User::factory()->create());

        // Create a new recipe to update later in the test and check against.
        Recipe::factory()->for($user)->create();

        // Instantiate our Livewire test using the EditRecipe class and set the request data.
        Livewire::test(MyRecipes::class)->call('render')->assertStatus(200);
    }

    /**
     * An unauthenticated user may not access my-recipes page.
     *
     * @return void
     */
    public function testUnauthenticatedUsersCannotAccessMyRecipesPage()
    {
        // assert the user is redirected
        $this->get('/recipes/my-recipes')->assertStatus(302);
    }

    /**
     * An unauthenticated user may not access edit recipe page.
     *
     * @return void
     */
    public function testUnauthenticatedUsersCannotAccessEditRecipePage()
    {
        $recipe = Recipe::factory()->for(User::factory()->create())->create();

        $this->get(route('recipes.edit', $recipe))->assertStatus(302);
    }

    /**
     * An unauthenticated user may not access create recipe page.
     *
     * @return void
     */
    public function testUnauthenticatedUsersCannotAccessCreateRecipePage()
    {
        $this->get(route('recipes.create'))->assertStatus(302);
    }

    /**
     * An unauthenticated user may access browse recipes page.
     *
     * @return void
     */
    public function testUnauthenticatedUsersCanAccessBrowseRecipesPage()
    {
        $this->get(route('recipes.browse'))->assertStatus(200);
    }
}
