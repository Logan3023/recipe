<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('My Recipes') }}
        </h2>
    </x-slot>
    {{-- The whole world belongs to you. --}}
    <div class="p-4 w-full">
        @foreach($recipes as $recipe)
            <x-recipe-card :recipe="$recipe">
            </x-recipe-card>
        @endforeach
    </div>
</div>
