<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Browse Recipes') }}
        </h2>
    </x-slot>
    @foreach($recipes as $recipe)
    <x-recipe-card :recipe="$recipe"></x-recipe-card>
    @endforeach

        {{$recipes->links()}}

</div>
