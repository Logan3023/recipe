<div>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit Recipe') }}
        </h2>
    </x-slot>
    <div class="container center items-center mx-auto">
        <button wire:click.prevent="destroy()"
                class="w-full text-center flex ml-0 text-white bg-red-500 border-0 py-2 px-6 focus:outline-none hover:bg-red-600 rounded">
            <span class="mx-auto">Delete Recipe</span></button>
        <form wire:submit.prevent="update">
            <div class="flex flex-wrap -mx-3 mb-6">
                <div class="w-full px-3 mb-6 md:mb-0">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                           for="grid-first-name">
                        Recipe Title
                    </label>
                    <input
                        wire:model="title"
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                        id="grid-title" type="text" placeholder="title">
                    @error('title')
                    <p class="text-red-500 text-xs italic">Please fill out this field.</p>
                    @enderror
                </div>
                <div class="w-full px-3 mb-6 md:mb-0">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                           for="grid-first-name">
                        Prep Time (minutes)
                    </label>
                    <input
                        wire:model="prep_time"
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                        id="grid-title" type="number" placeholder="15">
                    @error('prep_time')
                    <p class="text-red-500 text-xs italic">Please fill out this field.</p>
                    @enderror
                </div>
                <div class="w-full px-3 mb-6 md:mb-0">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                           for="grid-first-name">
                        Cook Time (minutes)
                    </label>
                    <input
                        wire:model="cook_time"
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-red-500 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white"
                        id="grid-title" type="number" placeholder="15">
                    @error('cook_time')
                    <p class="text-red-500 text-xs italic">Please fill out this field.</p>
                    @enderror
                </div>
                <div class="w-full px-3">
                    <label class="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                           for="grid-last-name">
                        Recipe Description
                    </label>
                    <textarea
                        wire:model="description"
                        class="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                        id="grid-last-name" placeholder="Doe">
                    </textarea>
                    @error('description')
                    <p class="text-red-500 text-xs italic">Please fill out this field.</p>
                    @enderror
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Add some tags that describe your recipe</label>
                        <div wire:ignore>
                            <select id="tag-dropdown" class="form-control" multiple wire:model="tags">
                                @foreach($tagList as $tag)
                                    <option value="{{$tag->name}}">{{ $tag->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        @error('tags')
                        <p class="text-red-500 text-xs italic">Please fill out this field.</p>
                        @enderror
                    </div>
                </div>
            </div>
            {{--Add Steps--}}
            <div class="grid-rows-1">
                <div class="items-center">
                    <button
                        class="w-full text-center flex ml-0 text-white bg-purple-500 border-0 py-2 px-6 focus:outline-none hover:bg-purple-600 rounded"
                        wire:click.prevent="addStep({{count($steps)}})"><span class="mx-auto">Add A Step</span></button>
                </div>
            </div>
            @foreach($steps as $key => $value)
                <div class="flex flex-auto my-2">
                    <input wire:model="steps.{{ $key }}"
                           type="text"
                           class="w-full rounded-lg border-transparent flex-1 appearance-none border border-gray-300 py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent"
                    />
                    <a wire:click.prevent="remove({{ $key }}) cursor-pointer focus:outline-none hover:text-red-700">
                        <svg class="mt-3 fill-current text-red-500 cursor-pointer hover:text-red-300"
                             xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 18 18">
                            <path
                                d="M14.53 4.53l-1.06-1.06L9 7.94 4.53 3.47 3.47 4.53 7.94 9l-4.47 4.47 1.06 1.06L9 10.06l4.47 4.47 1.06-1.06L10.06 9z"></path>
                        </svg>
                    </a>
                </div>
            @endforeach
            @if(count($steps))
                <button wire:click.prevent="update()"
                        class="w-full text-center flex ml-0 text-white bg-green-500 border-0 py-2 px-6 focus:outline-none hover:bg-purple-600 rounded">
                    <span class="mx-auto">Save Updates</span></button>
            @endif
        </form>
    </div>
</div>

@push('scripts')
    <script>
        $(document).ready(function () {
            $('#tag-dropdown').select2();
            $('#tag-dropdown').on('change', function (e) {
                let data = $(this).val();
            @this.set('tags', data);
            });
            // We may emit an event in the future.
            // window.livewire.on('tagStore', () => {});
        });
    </script>
@endpush
