<?php

namespace App\Http\Livewire;

use App\Models\Recipe;
use App\Models\RecipeStep;
use App\Models\Tag;
use Livewire\Component;

class EditRecipe extends Component
{
    public $recipe, $tags, $description, $title, $cook_time, $prep_time, $steps, $stepCount;

    protected array $rules = [
        'tags' => 'sometimes|required|array',
        'title' => 'sometimes|required|string|min:4|max:255',
        'prep_time' => 'sometimes|required|integer|min:1',
        'cook_time' => 'sometimes|required|integer|min:1',
        'description' => 'sometimes|required|string',
        'steps' => 'sometimes|required|array'
    ];

    public function mount(Recipe $recipe)
    {
        $this->recipe = $recipe;
        $this->title = $recipe->title;
        $this->cook_time = $recipe->cook_time;
        $this->prep_time = $recipe->prep_time;
        $this->description = $recipe->description;
        $this->tags = $recipe->tags()->pluck('name');
    }

    public function hydrateSteps()
    {
        if (empty($this->steps)) {

            $steps = $this->recipe->steps()->orderBy('order')->get(['order', 'description']);

            $steps->each(function ($step) {
                $this->steps[$step->order] = $step->description;
            });

            $this->stepCount = count($steps);
        }
    }

    public function addStep($count)
    {
        ++$count;

        $this->steps[$count + 1] = "";
        $this->steps = array_values($this->steps);
    }

    public function remove($i)
    {
        unset($this->steps[$i]);
        $this->steps = array_values($this->steps);
    }

    public function update()
    {
        // Validate the request data
        $validatedData = $this->validate();

        // Update the Recipe Model
        $this->recipe->update($validatedData);

        // Sync all the tags for the Recipe Model
        $this->recipe->syncTags($validatedData['tags']);

        // Delete pre-existing steps
        $this->recipe->steps()->each(function ($step){
           $step->delete();
        });

        // Loop over steps and create them in the new order
        foreach($this->steps as $order => $description){
            RecipeStep::create([
                'recipe_id' => $this->recipe->id,
                'order' => $order,
                'description' => $description
            ]);
        }
        // Redirect the User to the Recipe's Show page
        return redirect()->to("/recipes/edit/1");
    }

    public function destroy()
    {
        $this->recipe->steps()->each(function ($step){
            $step->delete();
        });

        $this->recipe->tags()->each(function ($tag){
            $tag->delete();
        });

        $this->recipe->delete();

        return redirect()->to("/recipes/my-recipes");
    }

    public function render()
    {
        $this->hydrateSteps();

        return view('livewire.edit-recipe', [
            'tagList' => Tag::all('name')
        ]);
    }
}
