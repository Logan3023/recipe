<?php

namespace App\Http\Livewire;

use App\Models\Recipe;
use Livewire\Component;

class ListRecipe extends Component
{
    public function render()
    {
        return view('livewire.list-recipe', ['recipes' => Recipe::with(['tags', 'user'])->paginate(10)]);
    }
}
