<?php

namespace App\Http\Livewire;

use App\Models\Recipe;
use Livewire\Component;

class MyRecipes extends Component
{
    public function render()
    {
        return view('livewire.my-recipes', [
            'recipes' => auth()->user()->recipes()->with('tags')->latest()->get()
        ]);
    }
}
