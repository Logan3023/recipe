<?php

namespace App\Http\Livewire;

use App\Models\Recipe;
use App\Models\RecipeStep;
use App\Models\Tag;
use Livewire\Component;

class CreateRecipe extends Component
{
    public $tags, $description, $title, $cook_time, $prep_time, $user_id, $steps;
    public $inputs = [];
    public $i = 1;
    public $stepOrder = 1;

    protected $rules = [
        'tags' => 'required',
        'title' => 'required',
        'prep_time' => 'required',
        'cook_time' => 'required',
        'description' => 'required',
        'steps.*' => 'required'
    ];

    public function add($i)
    {
        $i = $i++;
        $this->i = $i;
        array_push($this->inputs ,$i);
    }

    public function remove($i)
    {
        unset($this->inputs[$i]);
        unset($this->steps[$i]);
    }

    public function store()
    {
        // Validate the request data
        $validatedData = collect($this->validate());

        // make the new recipe and associate the authenticated user
        $recipe = Recipe::make(
            $validatedData->except('tags', 'steps')->toArray())
            ->user()
            ->associate(auth()->user()
            );

        // persist the new recipe
        $recipe->save();

        // Loop over the tags provided in the form and create any new tags that may not already exist.
        // We also attach all the tags provided to the newly created recipe.
        collect($validatedData['tags'])->each(function ($tag) use ($recipe) {

            $tag = Tag::firstOrCreate(['name' => $tag]);

            $recipe->tags()->attach($tag->id);

        });

        collect($validatedData['steps'])->each(function ($step) use ($recipe) {

           RecipeStep::firstOrCreate([
                'description' => $step,
                'order' => $this->stepOrder,
                'recipe_id' => $recipe->id
            ]);

            ++$this->stepOrder;
        });

        // Redirect the user back to the recipe creation page for now.
        return redirect()->to('/recipes/create');
    }

    public function render()
    {
        return view('livewire.create-recipe', [
            // Provide all the existing Tags to the component for select2
            'tagList' => Tag::all('name')
        ]);
    }
}
