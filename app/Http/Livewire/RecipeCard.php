<?php

namespace App\Http\Livewire;

use App\Models\Recipe;
use Illuminate\Contracts\View\View;
use Livewire\Component;

class RecipeCard extends Component
{
    public Recipe $recipe;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(Recipe $recipe)
    {
        $this->recipe = $recipe;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.recipe-card-component', ['recipe' => $this->recipe]);
    }
}
