<?php


namespace App\Traits;


use App\Models\Tag;

trait HasTags
{
    public function syncTags(array $tags)
    {
        foreach ($tags as $tag) {
            $tag = Tag::firstOrCreate(['name' => $tag]);

            $results[$tag->id] = $tag->id;
        }

        $this->tags()->sync($results);
    }
}
